//
//  Constants.swift
//  GitHubApp
//
//  Created by Łukasz Michalski on 03/02/2020.
//  Copyright © 2020 Łukasz Michalski. All rights reserved.
//

import Foundation

struct CellIdentifier {
    static let repository = "RepositoryCollectionViewCell"
}

struct CellRepositoryString {
    static let name = "Nazwa repozytorium:"
    static let description = "Opis:"
    static let createDate = "Data utworzenia:"
    static let lastUpdateDate = "Data ostatniej modyfikacji:"
}

struct ListRepositoriesString {
    static let placeholderTextField = "Nazwa użytkownika"
    static let defaultRepositoryName = "facebook"
    static let infoHeader = "Podaj nazwa użytkownika GitHub"
    static let repositoryNotFound = "Błędna nazwa repozytorium lub repozytorium jest puste"
}
