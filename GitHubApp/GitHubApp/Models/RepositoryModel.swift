//
//  RepositoreModel.swift
//  GitHubApp
//
//  Created by Łukasz Michalski on 28/01/2020.
//  Copyright © 2020 Łukasz Michalski. All rights reserved.
//

import Foundation

class Repository: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case description = "description"
    }
    
    var id: Int = 0
    var name: String = ""
    var createdAt: String? = ""
    var updatedAt: String? = ""
    var description: String? = ""
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        createdAt = try container.decode(String?.self, forKey: .createdAt)
        updatedAt = try container.decode(String?.self, forKey: .updatedAt)
        description = try container.decode(String?.self, forKey: .description)
    }
}
