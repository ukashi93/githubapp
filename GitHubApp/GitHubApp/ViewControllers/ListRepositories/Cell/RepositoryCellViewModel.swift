//
//  RepositorieCellViewModel.swift
//  GitHubApp
//
//  Created by Łukasz Michalski on 02/02/2020.
//  Copyright © 2020 Łukasz Michalski. All rights reserved.
//

import Foundation

class RepositoryCellViewModel {
    var repositoryName: String
    var createDate: String
    var lastUpdate: String
    var description: String
    
    init(repository: Repository) {
        self.repositoryName = repository.name
        self.createDate = repository.createdAt?.getDateFromDateZone() ?? "-"
        self.lastUpdate = repository.updatedAt?.getDateFromDateZone() ?? "-"
        self.description = repository.description ?? "-"
    }
}
