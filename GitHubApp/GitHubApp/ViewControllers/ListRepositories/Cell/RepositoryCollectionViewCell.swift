//
//  RepositorieCollectionViewCell.swift
//  GitHubApp
//
//  Created by Łukasz Michalski on 28/01/2020.
//  Copyright © 2020 Łukasz Michalski. All rights reserved.
//

import UIKit

class RepositoryCollectionViewCell: UICollectionViewCell {
    
    var nameValueLabel: CustomLabel = {
        let label = CustomLabel()
        label.initValueLabel(fontSize: 18)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.7
        label.numberOfLines = 2
        return label
    }()
    
    var descriptionValueLabel: CustomLabel = {
        let label = CustomLabel()
        label.initValueLabel(fontSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    var createDateValueLabel: CustomLabel = {
        let label = CustomLabel()
        label.initValueLabel(fontSize: 15)
        return label
    }()
    
    var lastUpdateDateValueLabel: CustomLabel = {
        let label = CustomLabel()
        label.initValueLabel(fontSize: 15)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(model: RepositoryCellViewModel) {
        self.nameValueLabel.text = model.repositoryName
        self.descriptionValueLabel.text = model.description
        self.createDateValueLabel.text = model.createDate
        self.lastUpdateDateValueLabel.text = model.lastUpdate
    }
    
    private func setupView() {
        self.backgroundColor = .cellBackgroud
        self.layer.cornerRadius = 5
        self.addShadow(color: .white, offset: CGSize(width: 0.3, height: 0), opacity: 0.8, radius: 5)
        
        let leftView = UIView()
        let rightView = UIView()
        let nameLabel = CustomLabel()
        let descriptionLabel = CustomLabel()
        let createDateLabel = CustomLabel()
        let lastUpdateDateLabel = CustomLabel()
        
        
        nameLabel.initDescriptionLabel(CellRepositoryString.name)
        descriptionLabel.initDescriptionLabel(CellRepositoryString.description)
        createDateLabel.initDescriptionLabel(CellRepositoryString.createDate)
        lastUpdateDateLabel.initDescriptionLabel(CellRepositoryString.lastUpdateDate)
        
        addSubview(leftView)
        addSubview(rightView)
        
        leftView.addSubview(nameLabel)
        leftView.addSubview(descriptionLabel)
        leftView.addSubview(createDateLabel)
        leftView.addSubview(lastUpdateDateLabel)
        
        rightView.addSubview(nameValueLabel)
        rightView.addSubview(descriptionValueLabel)
        rightView.addSubview(createDateValueLabel)
        rightView.addSubview(lastUpdateDateValueLabel)
        
        leftView.leftToSuperview()
        leftView.topToSuperview()
        leftView.bottomToSuperview()
        leftView.width(self.contentView.frame.width/2)
        
        nameLabel.top(to: leftView, offset: 10)
        nameLabel.left(to: leftView, offset: 10)
        nameLabel.rightToSuperview()
        nameLabel.height(to: nameValueLabel)
        
        descriptionLabel.leading(to: nameLabel)
        descriptionLabel.trailing(to: nameLabel)
        descriptionLabel.height(to: descriptionValueLabel)
        descriptionLabel.topToBottom(of: nameLabel)
        
        createDateLabel.leading(to: nameLabel)
        createDateLabel.trailing(to: nameLabel)
        createDateLabel.topToBottom(of: descriptionLabel)
        
        lastUpdateDateLabel.leading(to: nameLabel)
        lastUpdateDateLabel.trailing(to: nameLabel)
        lastUpdateDateLabel.topToBottom(of: createDateLabel)
        lastUpdateDateLabel.bottomToSuperview(offset: -20)
        
        rightView.topToSuperview()
        rightView.rightToSuperview()
        rightView.bottomToSuperview()
        rightView.width(self.contentView.frame.width/2)
        
        nameValueLabel.top(to: rightView, offset: 10)
        nameValueLabel.left(to: rightView, offset: 10)
        nameValueLabel.rightToSuperview(offset: -10)
        
        descriptionValueLabel.leading(to: nameValueLabel)
        descriptionValueLabel.trailing(to: nameValueLabel)
        descriptionValueLabel.topToBottom(of: nameValueLabel)
        
        createDateValueLabel.leading(to: nameValueLabel)
        createDateValueLabel.trailing(to: nameValueLabel)
        createDateValueLabel.topToBottom(of: descriptionValueLabel)
        createDateValueLabel.height(18)
        
        lastUpdateDateValueLabel.leading(to: nameValueLabel)
        lastUpdateDateValueLabel.trailing(to: nameValueLabel)
        lastUpdateDateValueLabel.topToBottom(of: createDateValueLabel)
        lastUpdateDateValueLabel.height(18)
        lastUpdateDateValueLabel.bottomToSuperview(offset: -20)
    }
}
