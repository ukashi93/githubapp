//
//  ListRepositoriesViewModel.swift
//  GitHubApp
//
//  Created by Łukasz Michalski on 28/01/2020.
//  Copyright © 2020 Łukasz Michalski. All rights reserved.
//

import Foundation

public class ListRepositoriesViewModel {
    
    var repositories: [Repository] = []
    
    func fetchRepositories(_ nameRepository: String, completionBlock: @escaping (_ succes: Bool)->()) {
        if nameRepository.isValidRepository {
            ConnectionManager().simpleRequest(urlString: (ConnectionManager.URLs.baseURL + String(format: ConnectionManager.URLs.publicRepos, nameRepository.removeWhitespace()))) { [weak self] (data) in
                guard let self = self else { return }
                let repositories = try? JSONDecoder().decode([Repository].self, from: data)
                self.repositories = repositories ?? []
                completionBlock(true)
            }
        } else {
            self.repositories = []
            completionBlock(false)
        }
    }
    
}
