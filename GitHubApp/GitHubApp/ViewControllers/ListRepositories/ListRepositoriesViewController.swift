//
//  ListRepositoriesViewController.swift
//  GitHubApp
//
//  Created by Łukasz Michalski on 28/01/2020.
//  Copyright © 2020 Łukasz Michalski. All rights reserved.
//

import UIKit
import TinyConstraints

class ListRepositoriesViewController: UIViewController, UITextFieldDelegate {
    
    var viewModel = ListRepositoriesViewModel()
    let headerViewHeight: CGFloat = 100
    
    let headerView: UIView = {
        let view = UIView()
        view.backgroundColor = .darkBackground
        return view
    }()
    
    let repositoryNameTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .white
        textField.borderStyle = .roundedRect
        textField.placeholder = ListRepositoriesString.placeholderTextField
        textField.text = ListRepositoriesString.defaultRepositoryName
        return textField
    }()
    
    let infoHeaderLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.text = ListRepositoriesString.infoHeader
        return label
    }()
    
    let repositoryNotFoundLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.text = ListRepositoriesString.repositoryNotFound
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    let emojiImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "sad_emoji")
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .white
        return imageView
    }()
    
    var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.repositoryNameTextField.delegate = self
        collectionView.reloadData()
        let dismissKeyboardWhenTapped: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(dismissKeyboardWhenTapped)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setRepository(nameRepository: repositoryNameTextField.text ?? "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func initCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.itemSize = UICollectionViewFlowLayout.automaticSize
        layout.estimatedItemSize = CGSize(width: UIScreen.main.bounds.width - 20, height: 80)
        layout.minimumLineSpacing = 25
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(RepositoryCollectionViewCell.self, forCellWithReuseIdentifier: CellIdentifier.repository)
        collectionView.backgroundColor = .darkBackground
        self.view.addSubview(collectionView)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == repositoryNameTextField {
            textField.resignFirstResponder()
            setRepository(nameRepository: textField.text ?? "")
            return false
        }
        return true
    }
    
    @objc func dismissKeyboard() {
        resignFirstResponder()
        view.endEditing(true)
    }
    
    func setRepository(nameRepository: String) {
        let loader = LoaderView()
        repositoryNotFoundLabel.isHidden = true
        emojiImage.isHidden = true
        loader.showActivityIndicator(uiView: self.view)
        self.view.layoutIfNeeded()
        viewModel.fetchRepositories(nameRepository, completionBlock: { [weak self] _ in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                loader.hideActivityIndicator(uiView: self.view)
            }
        })
    }
    
    func setupView() {
        repositoryNotFoundLabel.isHidden = true
        emojiImage.isHidden = true
        view.backgroundColor = .darkBackground
        
        view.addSubview(headerView)
        headerView.addSubview(repositoryNameTextField)
        headerView.addSubview(infoHeaderLabel)
        view.addSubview(repositoryNotFoundLabel)
        view.addSubview(emojiImage)
        
        headerView.edgesToSuperview(excluding: .bottom, usingSafeArea: true)
        headerView.height(headerViewHeight)
        
        infoHeaderLabel.top(to: headerView, offset: headerViewHeight / 4)
        infoHeaderLabel.left(to: headerView, offset: 15)
        infoHeaderLabel.right(to: headerView, offset: -15)
        infoHeaderLabel.height(20)
        
        repositoryNameTextField.topToBottom(of: infoHeaderLabel, offset: 10)
        repositoryNameTextField.left(to: headerView, offset: 15)
        repositoryNameTextField.right(to: headerView, offset: -15)
        repositoryNameTextField.height(40)
        
        initCollectionView()
        collectionView.topToBottom(of: headerView, offset: 10)
        collectionView.leftToSuperview()
        collectionView.rightToSuperview()
        collectionView.bottomToSuperview(usingSafeArea: true)
        
        repositoryNotFoundLabel.leading(to: view, offset: 20)
        repositoryNotFoundLabel.trailing(to: view, offset: -20)
        repositoryNotFoundLabel.centerY(to: view, offset: -100)
        
        emojiImage.topToBottom(of: repositoryNotFoundLabel, offset: 20)
        emojiImage.centerXToSuperview()
        emojiImage.width(view.frame.width / 2)
        emojiImage.heightToWidth(of: emojiImage)
    }
    
}

extension ListRepositoriesViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel.repositories.isEmpty {
            collectionView.isHidden = true
            repositoryNotFoundLabel.isHidden = false
            emojiImage.isHidden = false
            return 0
        }
        
        collectionView.isHidden = false
        repositoryNotFoundLabel.isHidden = true
        emojiImage.isHidden = true
        return viewModel.repositories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.repository, for: indexPath as IndexPath) as! RepositoryCollectionViewCell
        let cellModel = RepositoryCellViewModel(repository: viewModel.repositories[indexPath.row])
        cell.setup(model: cellModel)
        return cell
    }
}
