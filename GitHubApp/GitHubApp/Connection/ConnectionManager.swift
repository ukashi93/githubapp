//
//  ConnectionManager.swift
//  GitHubApp
//
//  Created by Łukasz Michalski on 29/01/2020.
//  Copyright © 2020 Łukasz Michalski. All rights reserved.
//

import Foundation


public class ConnectionManager {
    
    func simpleRequest(urlString: String, completion: @escaping (Data) -> Void) -> Void {
        if let url = URL(string: urlString) {
            let requestURL = url
            let request = URLRequest(url: requestURL)
            
            let requestTask = URLSession.shared.dataTask(with: request) {
                (data: Data?, response: URLResponse?, error: Error?) in
                
                if(error != nil) {
                    print("Error: \(String(describing: error))")
                } else{
                    guard let data = data else { return }
                    completion(data);
                }
                
            }
            requestTask.resume()
        }
    }
}
