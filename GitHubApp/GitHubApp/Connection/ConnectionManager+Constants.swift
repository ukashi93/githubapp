//
//  ConnectionManager+Constants.swift
//  GitHubApp
//
//  Created by Łukasz Michalski on 29/01/2020.
//  Copyright © 2020 Łukasz Michalski. All rights reserved.
//

import Foundation

extension ConnectionManager {
    
    enum URLs {
        static let baseURL = "https://api.github.com/"
        static let publicRepos = "users/%@/repos"
    }
}
