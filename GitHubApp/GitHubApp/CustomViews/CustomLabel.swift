//
//  DescriptionLabel.swift
//  GitHubApp
//
//  Created by Łukasz Michalski on 03/02/2020.
//  Copyright © 2020 Łukasz Michalski. All rights reserved.
//

import UIKit

class CustomLabel: UILabel {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func initDescriptionLabel(_ name: String) {
        self.text = name
        self.textColor = .grayFont
        self.font = .systemFont(ofSize: 15)
        self.adjustsFontSizeToFitWidth = true
        self.minimumScaleFactor = 0.7
    }
    
    func initValueLabel(fontSize: CGFloat = 15) {
        self.textColor = .black
        self.font = .boldSystemFont(ofSize: fontSize)
    }
}
