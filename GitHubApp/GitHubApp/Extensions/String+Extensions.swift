//
//  String+Extension.swift
//  GitHubApp
//
//  Created by Łukasz Michalski on 03/02/2020.
//  Copyright © 2020 Łukasz Michalski. All rights reserved.
//

import Foundation

extension String {
    
    func getDateFromDateZone() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MM-yyyy"
        
        if let date = dateFormatterGet.date(from: self) {
            return (dateFormatterPrint.string(from: date))
        } else {
            return "-"
        }
    }
    
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(string: " ", replacement: "")
    }
    
    var isValidRepository: Bool {
           return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
}
