//
//  UIColor+Extensions.swift
//  GitHubApp
//
//  Created by Łukasz Michalski on 31/01/2020.
//  Copyright © 2020 Łukasz Michalski. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let darkBackground = #colorLiteral(red: 0.1605761051, green: 0.1642630696, blue: 0.1891490221, alpha: 1)
    static let dimmedViewBackground = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2956646127)
    static let cellBackgroud = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1)
    static let grayFont = #colorLiteral(red: 0.3921568627, green: 0.3921568627, blue: 0.3921568627, alpha: 1)
}
