//
//  UIView+Extensions.swift
//  GitHubApp
//
//  Created by Łukasz Michalski on 03/02/2020.
//  Copyright © 2020 Łukasz Michalski. All rights reserved.
//

import UIKit

extension UIView {
    
    func addShadow(color: UIColor, offset: CGSize, opacity: Float, radius: CGFloat) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
        self.layer.masksToBounds = false
    }
}
